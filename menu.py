import juego

class menu(object):
    def __init__(self):
        self.menu_juego()
    def menu_juego(self):
        print("------------------------------")
        print("Bienvenido al juego de la vida")
        print("------------------------------")
        print("Reglas del juego")
        print("1.Una celda muerta con tres vecinos vivos revive.")
        print("2.Una celda viva con 2 o tres vecinos vivos permanece viva.")
        print("3.Una celda con menos de 2 o más de tres vecinos vivos muere.")
        print("")
        
        while True:
            print("ingresar 'salir' para salir del juego")
            print("presionar 'enter' para ingresar al juego")
            try:
                valor5 = input("ingresar: ")
            
            except(EOFError, KeyboardInterrupt,ValueError, TypeError):
                     print("Dato invalido")
                     continue
            if valor5 == "":
                while True:
                    print("")
                    print("ingresar tamaño de tablero")
                    print("ingresar 0 para cargar tablero")
                    try:
                        tamaño1 = int(input('ingresar numero: '))
                        
                        juego1=juego.juego_de_la_vida(tamaño1)
                        tamaño2 =tamaño1
                        if tamaño1 < 0: 
                             print("Por favor ingrese un numero mayor a 0.")
                             continue
                        elif tamaño1 == 0:
                            while True:
                                try:
                                    print("")
                                    archivo = input("ingrese nombre/ruta del archivo: ")
                                    juego1.cargar_tablero(archivo)
                                    break
                                except (EOFError, KeyboardInterrupt,KeyError):
                                    print("archivo invalido")
                                    continue
                            break    
                        else:  
                            
                             break
                    except(EOFError, KeyboardInterrupt,ValueError, TypeError):
                         print("Por favor ingrese un numero mayor a 0")
                         continue
                    
                if tamaño2 != 0:
                    print("")        
                    print("Introducir celulas vivas:")
                    print("ingresar 1 para empezar con un patrón al azar")
                    print("ingresar 2 para indicar la fila y columna de cada celda viva.")
                    print("ingresar 3 para inciar modo vida estatica")
                    while True:
                        try:
                            print("")
                            valor = int(input('ingresar numero: '))
                        except(EOFError, KeyboardInterrupt,ValueError, TypeError):
                            print("Por favor ingrese un numero entre 1 y 3")
                            continue
                        if valor == 1:
                            while True:
                                 try:
                                     celdas_vivas_aleatorio = int(input('ingresar celdas vidas: '))
                                     
                                     if celdas_vivas_aleatorio <= 0 or celdas_vivas_aleatorio > tamaño1**2:
                                
                                        print("Por favor ingrese un numero mayor a 0,que no supere la cantidad de celdas totales")
                                
                                        continue
                                     else:
                                        juego1.iniciar_juego_aleatorio(celdas_vivas_aleatorio)
                                        break
                                 except (EOFError, KeyboardInterrupt,ValueError):
                                        print("Por favor ingrese un numero mayor a 0")
                                        continue
                            break      
                        elif valor == 2:
                            
                            while True:
                                    try:
                                        celdas_vivas = int(input('ingresar celdas vivas: '))
                                        if celdas_vivas <= 0 or celdas_vivas > tamaño1**2 : 
                                            print("Por favor ingrese un numero mayor a 0,que no supere la cantidad de celdas totales")
                                            continue
                                        else:
                                            juego1.ingresar_celdas_vivas(celdas_vivas)
                                            break
                            
                                    except (EOFError, KeyboardInterrupt,ValueError, TypeError):
                                        print("Por favor ingrese un numero entero mayor a 0")
                            
                                        continue
                            break
                        elif valor == 3:
                             while True:
                                 try:
                                     celdas_vivas_estatica = int(input('ingresar celdas vidas: '))
                                     if celdas_vivas_estatica <= 0 or celdas_vivas_estatica > tamaño1**2 : 
                                          print("Por favor ingrese un numero mayor a 0,que no supere la cantidad de celdas totales")
                                          continue
                                     else:
                                               
                                          juego1.jugar_vida_estatica(celdas_vivas_estatica)
                                          break
                                
                                 except (EOFError, KeyboardInterrupt,ValueError, TypeError):
                                            print("Por favor ingrese un numero entero mayor a 0.")
                                            continue
                            
                             break
                        else:
                            print("Por favor ingrese un numero entre 1 y 3")
                            continue
                while True:
                    print("")
                    print("ingresar 1 para avanzar en el juego")
                    print("ingresar 2 para salir al menu principal")
                    try:
                        valor1 = int(input('ingresar numero: '))
                    except(EOFError, KeyboardInterrupt,ValueError, TypeError):
                        print("Por favor ingrese un numero entre 1 y 2")
                        continue
                    if valor1 == 1:
                        while True:
                            print("")
                            print("presionar enter para continuar")
                            print("ingresar Ctrl-C para guardar el juego")
                            print("ingresar 'mod' para modificar alguna celda del juego")
                            print("ingresar 'salir' para salir del juego")
                            try:
                                valor2 = input('ingresar : ')
                                if valor2 == "":
                                  
                                   juego1.realizar_un_ciclo()
                                   if juego1.vida_estatica() == True or  juego1.vida_estatica_nivel2() == True:
                                       break
                                     
                                   continue
                                elif valor2 == "mod":
                                     fila = int(input('ingresar fila : '))
                                     columna = int(input('ingresar  colmna: '))
                                     valor3= input('ingresar valor : ')
                                     if fila<0 or columna<0 :
                                         print("ingrese fila y col mayor a 0")
                                         continue
                                     juego1.modificar_tablero(fila, columna, valor3)

                                     continue
                                elif valor2 == 'salir':
                                    break
                                else:
                                     print("valor ingresado no es correcto")
                            except KeyboardInterrupt:
                                while True:
                                    try:
                                        nombre_archivo = input('ingrese nombre/ruta del archivo: ')
                                        juego1.guardar_tablero(nombre_archivo)
                                        break
                                    except (EOFError, KeyboardInterrupt,KeyError):
                                            print("trayecto invalido")    
                                            continue
                            except TypeError:
                                 print("valor ingresado no es correcto.") 
                        continue
                        
                    elif valor1 == 2:
                         break
                    else:
                        print("Por favor ingrese un numero entre 1 y 2")
                        continue
            elif valor5 == 'salir':
               break
            else:
                print("ingrese salir o entrer")

if __name__=="__main__":
   menu1= menu()
            
       
