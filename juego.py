import random
import shelve
class juego_de_la_vida(object):
   matriz_vida_estatica = []
 
   def __init__(self,tamaño1):
        '''pre: tamaño_n1 (filas) y tamaño_m1 (columnas) deben ser positivos
            post: inicializa el juego de la vida con todas las células muertas, según la cantidad de filas y columnas que se indique
            parametros: tamaño_n1 y tamaño_m1 son int
            atributos: tamaño_n, que es la cantidad de filas del tablero; tamaño_m, que es la cantidad de columnas del tablero y matriz, que es el tablero.
            '''
        self.tamaño = tamaño1
        
        self.matriz=[['-' for i in range(self.tamaño)] for j in range(self.tamaño)] 
        self.imprimir_tablero(self.matriz)

   def iniciar_juego_aleatorio(self,celdas_vivas_aleatorio):
##        '''pre: celdas_vivas_aleatorio debe ser mayor a 0.
##            post: inicia un juego en modo aleatorio, poniendo celulas vivas en lugares al azar, según la cantidad de celdas vivas que se indiquen por parámetro.
##            parámetros: celdas_vivas_aleatorio, que es int.'''
        
        
        contador = 0
        while contador < celdas_vivas_aleatorio:

            fila = random.randint(0,self.tamaño -1)
            columna = random.randint(0,self.tamaño - 1)

            if self.matriz [fila][columna] == '-':
                            self.matriz[fila][columna] = '*'
                            contador +=1

            
        self.imprimir_tablero(self.matriz)

   def modificar_tablero(self, fila, columna, valor):
        try:
          if valor == '*' or valor == '-':
             
             if self.matriz[fila][columna] == '*' and valor =='*':
                print("Esa Celda ya estaba viva")
             else:
                self.matriz[fila][columna] = valor
             if self.matriz[fila][columna] == '-' and valor =='-':
                print("Esa Celda ya estaba muerta")
             else:
                self.matriz[fila][columna] = valor   
          else:
                print("valor ingresado no es correcto")
        except(EOFError, KeyboardInterrupt, IndexError):
            print("valor ingresado no es correcto, ingrese '*' o '-'")
        self.imprimir_tablero(self.matriz)
   def ingresar_celdas_vivas(self,celdas_vivas):
          #pre: celdas_vivas_aleatorio debe ser mayor a 0.
          #post: inicia un juego nuevo, poniendo celulas vivas en lugares que el jugador indique, según la cantidad de celdas vivas que se indiquen por parámetro.
          #parámetros: celdas_vivas, que es int.
    
        cont=0
        while True:
            if cont == celdas_vivas:
                print('se llenaron las celdas vivas')
                break
            
            else:
                try:
                    '''celda_viva_fil y celda_viva_col deben valer entre 0 y la longitud de la fila -1 o columna -1, respectivamente.'''
                    celda_viva_fil = int(input('ingresar fila de celda viva'))
                    celda_viva_col = int(input('ingresar columna de calda viva'))
                    if celda_viva_fil < 0 or celda_viva_col < 0:
                    
                        print("Por favor ingrese un numero mayor o iguala a 0")
                        continue
                    elif self.matriz[celda_viva_fil][celda_viva_col]== '*':
                        print("Esa celda ya estaba viva")
                        continue
                    else:
                        self.matriz[celda_viva_fil][celda_viva_col]= '*'
                        cont+=1
                except(EOFError, KeyboardInterrupt,ValueError, TypeError,IndexError):
                    print("Por favor ingrese un numero entero meyor a 0, que este en el rango de la matriz")
                    continue               
                               
        self.imprimir_tablero(self.matriz)        
   def analizar_vecinos(self, fila, columna):

        '''pre: fila y columna deben ser positivos y estar dentro del rango de la matriz. Este método será utilizado por el método realizar_un_ciclo()
            post: devuelve la cantidad de vecinos vivos que tiene una célula en específico, trabajando por índices.
            parametros: fila y columna son int
            retorna: vecinos_vivos, que es int.'''
        
        vecinos_vivos=0

        if fila-1 >= 0 and columna-1 >=0 : #Analiza si el vecino de arriba a la izquierda de la célula que estamos considerando existe.

            if self.matriz[fila-1][columna-1] == '*':
                vecinos_vivos+=1

                            
        if fila-1 >= 0: #Analiza si el vecino de a la izquierda de la célula que estamos considerando existe.

            if self.matriz[fila-1][columna] == '*': 
                vecinos_vivos+=1
                       
                           
        if fila-1 >= 0 and columna +1 <= self.tamaño-1: #Analiza si el vecino de arriba a la derecha de la célula que estamos considerando existe.

            if self.matriz[fila-1][columna+1] == '*':
                vecinos_vivos+=1
                       
                        
        if fila+1 <= self.tamaño-1 and columna -1 >= 0: #Analiza si el vecino de abajo a la izquierda de la célula que estamos considerando existe.

            if self.matriz[fila+1][columna-1] == '*':
                vecinos_vivos+=1
                     
                        
        if fila+1 <= self.tamaño-1: #Analiza si el vecino de a la derecha de la célula que estamos considerando existe.

            if self.matriz[fila+1][columna] == '*':
                vecinos_vivos+=1
                        
        if fila+1 <= self.tamaño-1 and columna +1 <= self.tamaño-1: #Analiza si el vecino de abajo a la derecha de la célula que estamos considerando existe.

            if self.matriz[fila+1][columna+1] == '*':
                vecinos_vivos+=1
                       
        if columna -1 >= 0: #Analiza si el vecino de abajo de la célula que estamos considerando existe.

            if self.matriz[fila][columna-1] == '*':
                vecinos_vivos+=1
                
        if columna +1 <= self.tamaño-1: #Analiza si el vecino de arriba de la célula que estamos considerando existe.

            if self.matriz[fila][columna+1] == '*':
                vecinos_vivos+=1

        return vecinos_vivos


   def realizar_un_ciclo(self):

        '''post: efectúa los ciclos del juego, reviviendo y/o matando células según las reglas del mismo lo indican'''
        self.matriz_vida_estatica.append(self.matriz)
        matriz_aux = [] #Creo matriz auxiliar a modo de efectuar los cambios en esta para luego copiarlos en la matriz tablero del juego, a modo de hacer los cambios mas seguros.

        for i in range(self.tamaño):
            columna_aux = [] #Creo columna auxiliar para llenarla y luego ponerla en la matriz auxiliar

            for j in range (self.tamaño):

                vecinos_vivos = self.analizar_vecinos(i, j) #Analizo los vecinos vivos para todas las celulas, iterativamente, una por una.

                if self.matriz[i][j] == '-': 
                    if vecinos_vivos == 3 : #Si la celula está muerta y tiene tres vecinos vivos, revive, por lo que pongo un * en la columna_aux. De lo contrario, pongo un -
                        columna_aux.append ('*')
                    else:
                        columna_aux.append ('-')
                        
                if self.matriz[i][j] == '*':
                    if vecinos_vivos == 2 or vecinos_vivos == 3: #Si la celula está viva y tiene dos o tres vecinos vivos, permanece viva, por lo que pongo un * en la columna_aux
                        columna_aux.append ('*')

                    if vecinos_vivos < 2 or vecinos_vivos > 3: #Si la celula está viva y tiene menos de dos o más de tres vecinos vivos, muere, por lo que pongo un - en la columna_aux.
                        columna_aux.append ('-')
                            
            matriz_aux.append(columna_aux) #Agrego la columna_aux a la matriz auxiliar
        
        self.matriz = matriz_aux #La matriz tablero recibe los cambios, asignandosele así la matriz auxiliar
        self.imprimir_tablero(self.matriz)
        print("")
   def guardar_tablero(self,nombre_archivo):
            print("")
            self.imprimir_tablero(self.matriz)
            print("Se guardo correctamente")
            print("")
            
            shlv = shelve.open(nombre_archivo)
              
            shlv["tablero"] = self.matriz
            
            shlv.close()
      
            
   def cargar_tablero(self, archivo):
            shlv = shelve.open(archivo)

            self.matriz = shlv["tablero"]

            shlv.close()
            
            cont1 = 0
            for i in self.matriz:
               cont1+=1
            self.imprimir_tablero(self.matriz)
            self.tamaño = cont1
           ## print(cont1)
          
           
   def imprimir_tablero (self, tablero):

        s = " "

        for fila in tablero:

            print (s.join(fila))
   
   def vida_estatica(self):
        """
        Se chequea si el tablero es estatico 
        """
        if len(self.matriz_vida_estatica) > 1:
            if self.matriz == self.matriz_vida_estatica[-1]:
                print('El tablero es estatico nivel 1.')
                return True
        else:
            return False
         
   def vida_estatica_nivel2(self):
      
      if len(self.matriz_vida_estatica) > 1:
         if self.matriz == self.matriz_vida_estatica[-2]:
            print('El tablero es estatico nivel 2.')
            return True
      else:
         return False      

   def jugar_vida_estatica(self, celdas_vivas_estatica):
      print("solo detecta vida estatica nivel 1 y 2")
      print("")
      cantidad=0
      
      # Primer argumento: cantidad total de celdas (filas * cols)
      # Segundo argumento: cantidad de celdas vivas
      for x in self.combinations(range(self.tamaño**2),celdas_vivas_estatica):
         self.matriz=[['-' for i in range(self.tamaño)] for j in range(self.tamaño)]
         for i in x:
            fila = int(i/self.tamaño)
            columna = i%self.tamaño
            self.matriz[fila][columna]='*'
            
         self.imprimir_tablero(self.matriz)
         print("")
         
         while True:
               self.realizar_un_ciclo()
               print("")
               if self.vida_estatica() == True or self.vida_estatica_nivel2() == True:
                  self.matriz_vida_estatica.append(self.matriz_vida_estatica)
                  break
         print(x)
         cantidad+=1
	  
      print("\nCantidad total de combinaciones: ",cantidad)


   def combinations(self,iterable, r):
      
        pool = tuple(iterable)
        n = len(pool)
        if r > n:
            return
        indices = list(range(r))
        yield tuple(pool[i] for i in indices)
        while True:
            for i in reversed(range(r)):
                if indices[i] != i + n - r:
                    break
            else:
                return
            indices[i] += 1
            for j in range(i+1, r):
                indices[j] = indices[j-1] + 1
            yield tuple(pool[i] for i in indices)

    
